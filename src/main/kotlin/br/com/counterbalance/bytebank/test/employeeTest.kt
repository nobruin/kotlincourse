import br.com.counterbalance.bytebank.model.Analyst
import br.com.counterbalance.bytebank.model.BonificationCalc
import br.com.counterbalance.bytebank.model.Director
import br.com.counterbalance.bytebank.model.Manager

fun employeeTest() {
    println("DigiBank")

    val employee = Analyst(name = "bruno", cpf = "11111111", wage = 1000.0)
    val manager = Manager(name = "bruno", cpf = "11111111", wage = 1000.0, password = 2222)
    val director = Director(name = "jessica", cpf = "222222222", wage = 4000.0, password = 111111, plr = 5000.0)

    val calc = BonificationCalc()


    employee.show()
    manager.show()
    director.show()

    calc.register(employee)
    calc.register(manager)
    calc.register(director)

    println("total de bonificações ${calc.total}")
}
