package br.com.counterbalance.bytebank.test

import br.com.counterbalance.bytebank.model.Address
import br.com.counterbalance.bytebank.model.Client
import br.com.counterbalance.bytebank.model.SavingAccount

fun testClientWithAddress() {
    val ben = Client(
        name = "Benjamin",
        cpf = "1111111",
        password = 1010,
        address = Address(
            street = "Engenheiro souza filho",
            number = 10,
            state = "RJ",
            city = "Rio de janeiro"
        )
    )

    val jessica = Client(name = "jessica", cpf = "121212121", password = 2323)

    val savingAccountJessica = SavingAccount(owner = jessica, numberAccount = 1212)
    val savingAccountBen = SavingAccount(owner = ben, numberAccount = 1211)

    savingAccountBen.deposit(10000.0)
    savingAccountJessica.deposit(20000.0)

    savingAccountJessica.transfer(1000.0, savingAccountBen)

    savingAccountBen.showInformation();
    savingAccountJessica.showInformation()
}