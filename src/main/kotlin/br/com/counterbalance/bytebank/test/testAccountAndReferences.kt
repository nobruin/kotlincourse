import br.com.counterbalance.bytebank.model.CheckingAccount
import br.com.counterbalance.bytebank.model.Client
import br.com.counterbalance.bytebank.model.SavingAccount

fun testAccountAndReferences() {
    val brunoAccount = CheckingAccount(
        Client("bruno","11111",1),
        1001
    );

    val jessicaAccount = SavingAccount(
        numberAccount = 1010,
        owner = Client("jessica", "111", 12)
    );

    brunoAccount.deposit(1000.0)

    jessicaAccount.deposit(1001.0)

    brunoAccount.showInformation()
    jessicaAccount.showInformation()

    println("deposito conta bruno")
    brunoAccount.deposit(13.0)
    println(brunoAccount.balance)

    println("deposito conta jessica")
    jessicaAccount.deposit(50.0)
    println(jessicaAccount.balance)

    println("transfera 10 reais da conta de bruno para jessica")
    brunoAccount.transfer(10.0, jessicaAccount)

    println(brunoAccount.balance)
    println(jessicaAccount.balance)
}