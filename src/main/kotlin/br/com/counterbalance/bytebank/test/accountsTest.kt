import br.com.counterbalance.bytebank.model.CheckingAccount
import br.com.counterbalance.bytebank.model.Client
import br.com.counterbalance.bytebank.model.SavingAccount

fun accountsTest() {
    val checkingAccount = CheckingAccount(
        owner = Client("bruno", "11111111111",1),
        numberAccount = 1001
    )

    checkingAccount.deposit(7000.0)

    val savingAccount = SavingAccount(
        owner = Client("jessica", "111111111", 12),
        numberAccount = 1002
    )

    savingAccount.deposit(8000.0)

    savingAccount.transfer(300.0, checkingAccount)
    checkingAccount.transfer(400.0, savingAccount)

    checkingAccount.showInformation()
    savingAccount.showInformation()
}

