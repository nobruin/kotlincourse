fun testBalance(value: Double) {
    when {
        value > 0.0 -> println("positivo")
        value == 0.0 -> println("neutro")
        else -> println("negativo")
    }
}