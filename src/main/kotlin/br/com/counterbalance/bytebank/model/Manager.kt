package br.com.counterbalance.bytebank.model

class Manager(
    name: String,
    cpf: String,
    wage: Double,
    val password: Int
) : Employee(
    name = name,
    cpf = cpf,
    wage = wage
) {
    override val bonification: Double
        get() {
            return wage
        }
}