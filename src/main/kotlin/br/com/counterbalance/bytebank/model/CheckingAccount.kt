package br.com.counterbalance.bytebank.model

class CheckingAccount(
    owner: Client,
    numberAccount: Int
) : Account(
    owner = owner,
    numberAccount = numberAccount
){

    override fun withdraw(value: Double) : Boolean{
        if(isWithdrawValid(value)){
            return false;
        }
        this.balance -= value + 0.1
        return true
    }

}