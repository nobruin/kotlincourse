package br.com.counterbalance.bytebank.model

class Director(
    name: String,
    cpf: String,
    wage: Double,
    override val password: Int,
    val plr: Double
) : Employee(
    name = name,
    cpf = cpf,
    wage = wage
), AuthInterface {
    override val bonification: Double
        get() {
            return  wage + plr
        }

    override fun oAuth(password: Int): Boolean {
        return this.password == password
    }
}
