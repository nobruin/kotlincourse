package br.com.counterbalance.bytebank.model

class SavingAccount(
    owner: Client,
    numberAccount: Int
) : Account(
    owner = owner,
    numberAccount = numberAccount
) {
    override fun withdraw(value: Double):Boolean {
        if(!isWithdrawValid(value)){
            throw Exception("o valor do saque é invalido")
        }

        this.balance -= value
        return true
    }
}