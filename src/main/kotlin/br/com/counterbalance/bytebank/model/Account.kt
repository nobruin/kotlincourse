package br.com.counterbalance.bytebank.model

import testBalance



abstract class Account(
    var owner: Client,
    val numberAccount: Int
    ) {
    var balance: Double = 0.0
        protected set

    companion object{
        var accountLength = 0
        private set
    }

   abstract fun withdraw(value: Double) : Boolean

   init {
       accountLength++
   }

    fun deposit(value: Double) {
        this.balance += value
    }

    fun showInformation(){

        println("Titular ${this.owner.name}")
        println("Numero ${this.numberAccount}")
        println("Saldo ${this.balance}")
        println("quantidades de contas $accountLength")

        testBalance(this.balance)
        println()
    }

    fun transfer(value: Double, destination: Account): Boolean {
        this.withdraw(value)
        destination.deposit(value);

        return true
    }

    protected fun isWithdrawValid(value: Double) : Boolean{
        if (this.balance < value){
            return false
        }

        return true
    }
}