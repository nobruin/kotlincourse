package br.com.counterbalance.bytebank.model

class Address(
    var street: String = "",
    var number: Int = 0,
    var zipCode: String = "",
    var city: String = "",
    var state: String = "",
    var complemente: String = ""
){
    override fun toString(): String {
        return """Address(street='$street', 
            number=$number, 
            zipCode='$zipCode', 
            city='$city', 
            state='$state', 
            complemente='$complemente')
            """.trimIndent()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Address

        if (street != other.street) return false
        if (number != other.number) return false
        if (zipCode != other.zipCode) return false
        if (city != other.city) return false
        if (state != other.state) return false
        if (complemente != other.complemente) return false

        return true
    }

    override fun hashCode(): Int {
        var result = street.hashCode()
        result = 31 * result + number
        result = 31 * result + zipCode.hashCode()
        result = 31 * result + city.hashCode()
        result = 31 * result + state.hashCode()
        result = 31 * result + complemente.hashCode()
        return result
    }


}