package br.com.counterbalance.bytebank.model

class Analyst(
    name: String,
    cpf: String,
    wage: Double
) : Employee(
    name = name,
    cpf = cpf,
    wage = wage
){
    override val bonification: Double
        get() = 1000.0 + 0.1
}