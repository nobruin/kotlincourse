package br.com.counterbalance.bytebank.model

abstract class Employee(
    val name: String,
    val cpf: String,
    val wage: Double
){
    abstract val bonification: Double

    fun show(){
        println("property :$name")
        println("property :$cpf")
        println("property :$wage")
        println("bonification ${this.bonification}")
        println()
    }
}