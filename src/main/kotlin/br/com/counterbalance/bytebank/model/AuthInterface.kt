package br.com.counterbalance.bytebank.model

interface AuthInterface {
    val password: Int

    fun oAuth(password: Int): Boolean{
        return this.password == password
    }
}