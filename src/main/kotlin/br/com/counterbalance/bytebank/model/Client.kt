package br.com.counterbalance.bytebank.model

class Client(
    val name: String,
    val cpf: String,
    override val password: Int,
    val address: Address = Address()) : AuthInterface {
}